#include <iostream>

class Adder
{
  int sum;

  public:

  Adder(int num1, int num2)
  {
    sum = num1 + num2;
  }

  void show()
  {
    std::cout << "Objected result is "
              << sum << std::endl;
  }
};

int main()
{
  int a = 5;
  int b = 10;

  Adder dodawacz(a, b);

  dodawacz.show();
}


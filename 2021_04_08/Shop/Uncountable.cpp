#include "Uncountable.h"

#include <iostream>
#include <string.h>

Uncountable::Uncountable(int sC, double k, int q, const char *n)
{
  std::cout << "parameter constructor uncount\n";
  strippedCat_ = sC;
  kgPrice_ = k;
  quantity_ = q;
  strcpy(name_, n);
} 

void Uncountable::show()
{
  std::cout << quantity_ << "kg\t" << name_ << "\t" << kgPrice_
            << "PLN/kg\t[" << strippedCat_ << "]\n";
}

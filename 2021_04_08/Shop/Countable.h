#ifndef COUNTABLE_H
#define COUNTABLE_H

#include "Base.h"


class Countable: public Base
{
  protected:

  double price_;
  int quantity_;

  static int count_;

  public:

  Countable();

  Countable(int sC, double p, int q, const char *n);

  void show();

  static void showTotal();
};

#endif

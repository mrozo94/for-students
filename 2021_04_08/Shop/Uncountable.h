#ifndef UNCOUNTABLE_H
#define UNCOUNTABLE_H

#include "Base.h"

class Uncountable: public Base
{
  double kgPrice_;
  double quantity_;

  public:

  Uncountable(int sC, double k, int q, const char *n);

  void show();
};

#endif

#include "Countable.h"

#include <iostream>
#include <string.h>

int Countable::count_ = 0;

Countable::Countable()
{
  std::cout << "default constructor count\n";
  strippedCat_ = -1;
  price_ = -1;
  quantity_ = -1;

  name_[0] = '\0';

  count_++;
}

Countable::Countable(int sC, double p, int q, const char *n)
{
  std::cout << "parameter constructor count\n";
  strippedCat_ = sC;
  price_ = p;
  quantity_ = q;
  strcpy(name_, n);

  count_++;
}

void Countable::show()
{
  std::cout << quantity_ << "x\t" << name_ << "\t" << price_
            << "PLN\t[" << strippedCat_ << "]\n";

//  showTotal();
}

void Countable::showTotal()
{
  std::cout << "Total amount of countable items is " << count_ << "\n";
}

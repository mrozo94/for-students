#ifndef BASE_H
#define BASE_H

class Base
{
  protected:

  int strippedCat_;
  char name_[128];

  public:

  virtual void show() = 0;
};

#endif

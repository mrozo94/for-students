#include <iostream>

int add(int num1, int num2)
{
  return num1 + num2;
}

int main()
{
  int a = 5;
  int b = 10;

  int sum = add(a, b);

  std::cout << "Structural result is "
    << sum << std::endl;
}

#ifndef GLOBALOBJECT_H
#define GLOBALOBJECT_H

#include <string>

using std::string;

class GlobalObject
{
  protected:

  int x_;
  int y_;

  string printName_;
  string printColor_;

  public:

  GlobalObject()
  {
    printName_ = " ";
    printColor_ = "";
  }

  void update(int x, int y)
  {
    x_ = x;
    y_ = y;
  }

  int getX() { return x_; }
  int getY() { return y_; }

  bool areYouHere(int x, int y)
  {
    return ((x_ == x) && (y_ == y));
  }

  void show()
  {
    std::cout << printName_;
  }

  string toString()
  {
    return printColor_ + printName_ + "\033[0m";
  }

  #if 0
  std::ostream& operator<<(std::ostream& os, GlobalObject &p)
  {
    os << p.printName_;

    return os;
  }
  #endif
};

#endif

#include <iostream>
#include <string>

#include "Tree.h"
#include "Opossum.h"

#include <vector>
#include <stdlib.h>
#include <time.h>

#define OBJECT_SIZE 5
#define MAP_SIZE    5

int main(int argc, char ** argv)
{
  srand (time(NULL));

  Tree trees[OBJECT_SIZE];

  for (int i = 0; i < OBJECT_SIZE; i++)
  {
    int x = rand() % MAP_SIZE;
    int y = rand() % MAP_SIZE;

    trees[i].update(x, y);
  }

  Opossum opossums[OBJECT_SIZE];

  for (int i = 0; i < OBJECT_SIZE; i++)
  {
    int x = rand() % MAP_SIZE;
    int y = rand() % MAP_SIZE;

    opossums[i].update(x, y);
  }

  //
  // Slow print.
  //

  std::cout << "SLOW PRINT\n";

  for (int y = 0; y < MAP_SIZE; y++)
  {
    for (int x = 0; x < MAP_SIZE; x++)
    {
      int flag = -1;

      for (unsigned int i = 0; i < OBJECT_SIZE; i++)
      {
        if (trees[i].areYouHere(x, y))
        {
          flag = 1;
          trees[i].show();
        }
        else if (opossums[i].areYouHere(x, y))
        {
          flag = 1;
          opossums[i].show();
        }
      }

      if (flag == -1)
      {
        std::cout << " ";
      }
    }

    std::cout << "\n";
  }

  //
  // String map.
  //

  string map[MAP_SIZE][MAP_SIZE];

  std::cout << "STRING MAP\n";

  for (int y = 0; y < MAP_SIZE; y++)
  {
    for (int x = 0; x < MAP_SIZE; x++)
    {
      map[x][y] = " ";
    }
  }

  for (int i = 0; i < OBJECT_SIZE; i++)
  {
    int x = trees[i].getX();
    int y = trees[i].getY();

    map[x][y] = trees[i].toString();

    x = opossums[i].getX();
    y = opossums[i].getY();

    map[x][y] = opossums[i].toString();
  }

  for (int y = 0; y < MAP_SIZE; y++)
  {
    for (int x = 0; x < MAP_SIZE; x++)
    {
      std::cout << map[x][y];
    }
    std::cout << "\n";
  }

  return 0;
}

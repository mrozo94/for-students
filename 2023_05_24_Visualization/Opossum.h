#ifndef OPOSSUM_H
#define OPOSSUM_H

#include "GlobalObject.h"

#include <string>

class Opossum: public GlobalObject
{
  public:

  Opossum()
  {
    printName_ = "O";
  }
};

#endif

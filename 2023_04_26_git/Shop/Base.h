#ifndef BASE_H
#define BASE_H

class Base
{
  protected:

  int strippedCat_;
  char name_[128];

  public:

  virtual void show() = 0;
};

class Countable: public Base
{
  protected:

  double price_;
  int quantity_;

  public:

  Countable()
  {
    std::cout << "default constructor count wazne\n";
    strippedCat_ = -1;
    price_ = -1;
    quantity_ = -1;

    name_[0] = '\0';
  }

  Countable(int sC, double p, int q, const char *n)
  {
    std::cout << "parameter constructor count wazne\n";
    strippedCat_ = sC;
    price_ = p;
    quantity_ = q;
    strcpy(name_, n);
  }

  void show()
  {
    std::cout << quantity_ << "x\t" << name_ << "\t" << price_
              << "PLN\t[" << strippedCat_ << "]\n";
  }
};


#endif

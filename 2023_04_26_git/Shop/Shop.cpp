#include <iostream>
#include <vector>
#include <string.h>
#include "Base.h"

class Uncountable: public Base
{
  double kgPrice_;
  double quantity_;

  public:

  Uncountable(int sC, double k, int q, const char *n)
  {
    strippedCat_ = sC;
    kgPrice_ = k;
    quantity_ = q;
    strcpy(name_, n);
  }

  void show()
  {
    std::cout << quantity_ << "kg\t" << name_ << "\t" << kgPrice_
              << "PLN/kg\t[" << strippedCat_ << "]\n";
  }
};

int main()
{
  Countable cookie(1, 2.3, 5, "cookie");
  Uncountable onion(2, 4.0, 100.5 , "onion");

//  cookie.show();
//  onion.show();

  int fullSize = 256;
  int size = 0;

  Base **allItems = (Base **) malloc(sizeof(Base *) * fullSize);

  allItems[size++] = &cookie;
  allItems[size++] = &onion;
  allItems[size++] = new Countable(3, 5, 10, "kinder niespodzianka");
  allItems[size++] = new Countable(31, 15, 125, "Ferero Rosze");
  allItems[size++] = new Uncountable(60, 7.32, 76.23, "benzynka");

  for (int i = 0; i < size; i++)
  {
    allItems[i] -> show();
  }

  delete allItems[2];
  delete allItems[3];
  delete allItems[4];

  free(allItems);


  return 0;
}

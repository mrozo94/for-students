#include <iostream>

#include "Parameters.h"

using std::cout;

int main(int argc, const char **argv)
{
  Parameters::showParameters();

  cout << "Main parameters:\n";
  for (int i = 0; i < argc; i++)
  {
    cout << "  argv[" << i << "] '" << argv[i] << "' \n";
  }
  cout << "\n\n";

  //
  // OPTION 1
  // 
  // Set parameters by using values set in code.
  //

  /*
  std::string a = "JakasNazwa";
  int b = 123;
  int c = 321;

  Parameters::setParameters(a, b, c);
  */

  //
  // OPTION 2
  //
  // Set parameters by using parameters passed to main.
  //

  //Parameters::setParameters(argc, argv);

  //
  // OPTION 3
  //
  // Set parameters by loading them from file
  // that name has been passed to the main.
  //

  //Parameters::setParameters(argv[1]);

  Parameters::showParameters();

  //
  // SAVING TO FILE OPTION
  //
  // Saving parameters to the file
  // that name has been passed to the main.
  //

  //Parameters::saveParameters(argv[2]);
}

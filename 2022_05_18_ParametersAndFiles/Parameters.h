#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <string>

class Parameters
{
  private:
  static std::string a_;
  static int b_;
  static int c_;

  public:

  static void setParameters(std::string a, int b, int c);
  static void setParameters(int argc, const char **argv);
  static void setParameters(const char *filename);

  static void showParameters();

  static void saveParameters(const char *filename);

  //
  // Those kind of getters for all static parameters are
  // super usefull, because you can include Parameters class
  // everywhere in your project and you assure hermetisation.
  //
  // Remember that there will be different input parameters and
  // different output parameters, what I wrote is just an example.
  //
  // And ofc for the output parameters you'll need to define
  // some additional methods to save them after simulation
  // or during them, but you'll figure that out.
  //

  static std::string getA()
  {
    return a_;
  }

  static int getB()
  {
    return b_;
  }

  static int getC()
  {
    return c_;
  }
};

#endif

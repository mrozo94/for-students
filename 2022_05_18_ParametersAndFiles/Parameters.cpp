#include "Parameters.h"

#include <cstddef>
#include <stdlib.h>
#include <fstream>
#include <iostream>

std::string Parameters::a_;
int Parameters::b_;
int Parameters::c_;

void Parameters::setParameters(std::string a, int b, int c)
{
  std::cout << "Setting parameters with passed values.\n";

  a_ = a;
  b_ = b;
  c_ = c;
}

void Parameters::setParameters(int argc, const char **argv)
{
  std::cout << "Setting parameters with argc/argv.\n";

  a_ = argv[1];
  b_ = atol(argv[2]);
  c_ = atol(argv[3]);
}

void Parameters::setParameters(const char *filename)
{
  std::cout << "Setting parameters from the file.\n";

  std::ifstream file;

  file.open(filename);

  if (!file.is_open())
  {
    std::cout << "ERROR! File not found!\n";
    return;
  }

  file >> a_;
  file >> b_;
  file >> c_;

  file.close();
}

void Parameters::showParameters()
{
  std::cout << "Stored parameters in 'Parameters':\n";

  std::cout 
      << "  a_ '" << a_ << "'\n"
      << "  b_ '" << b_ << "'\n"
      << "  c_ '" << c_ << "'\n\n";
}

void Parameters::saveParameters(const char *filename)
{
  std::cout << "Saving parameters to the file.\n";

  std::ofstream file;

  file.open(filename);

  if (!file.is_open())
  {
    std::cout << "ERROR! File not found!\n";
    return;
  }

  file << a_ << "\t";
  file << b_ << "\t";
  file << c_ << "\t";
  file << "\n";
  file.close();
}

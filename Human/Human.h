#ifndef HUMAN_H
#define HUMAN_H

#include <iostream>
#include <cstring>

class Human
{
  char name_[128];
  char sex_;
  int age_;
  int height_;

  public:

  char *getName()
  {
    return name_;
  }

  Human(const char *name, char sex, int age, int height)
  {
    strcpy(name_, name);

    if (!((sex == 'm') || (sex == 'M') ||
           (sex == 'f') || (sex == 'F')))
    {
      std::cout << "UWAGA! BŁĄD! \n";
      return;
    }
    
    sex_ = sex;
    age_ = age;
    height_ = height;
  }

  void show()
  {
    const char *sexName;

    if ((sex_ == 'f') || (sex_ == 'F'))
    {
      sexName = "girl";
    }
    else if ((sex_ == 'm') || (sex_ == 'M'))
    {
      sexName = "boy";
    }
    else
    {
      sexName = "other";
    }

    std::cout << "My name is " << name_ << ", "
              << "I'm a " << sexName << ". "
              << "I'm " << age_ << " years old and I'm "
              << height_ << "cm tall.\n";
  }

  bool operator >(const Human &tenDrugi)
  {
    if (((sex_ == 'f') || (sex_ == 'F')) &&
            ((tenDrugi.sex_ == 'm') || (tenDrugi.sex_ == 'M')))
    {
      return true;
    }

    if (((sex_ == 'm') || (sex_ == 'M')) &&
            ((tenDrugi.sex_ == 'f') || (tenDrugi.sex_ == 'F')))
    {
      return false;
    }

    if (age_ > tenDrugi.age_)
    {
      return true;
    }

    if (age_ < tenDrugi.age_)
    {
      return false;
    }

    if (height_ < tenDrugi.height_)
    {
      return true;
    }

    return false;
  }

};

#endif

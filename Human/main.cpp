#include "Human.h"

void pokazPare(Human h1, Human h2)
{
  std::cout << "Pierwszy człek to:\n";
  h1.show();
  std::cout << "Drugi człek to:\n";
  h2.show();
}

void ktoPierwszy(Human h1, Human h2)
{
  if (h1 > h2)
  {
    std::cout << h1.getName() << " pierwszy/a\n";
  }
  else
  {
    std::cout << h2.getName() << " pierwszy/a\n";
  }
}

int main()
{
  Human agata("Agata",'F',23,160);
  Human hubert("Hubert",'M',25,170);
  Human arek("Arek",'M',24,140);
  Human joanna("Aśka",'f',19,160);
  Human anna("Anna",'f',23,165);
  Human ogorek("Ogórek",'O',3,15);

  //
  // Pokazuje kto pierwszy podaje rękę.
  //

  ktoPierwszy(hubert, arek);
  ktoPierwszy(joanna, anna);
  ktoPierwszy(agata, joanna);
  ktoPierwszy(ogorek, agata);

  ogorek.show();
}
#if 0
  pokazPare(agata,arek);

  std::cout << "===\n";

  agata.show();
  arek.show();

  // Order of handshaking.
  if (agata > arek)
  {
    std::cout << "Agata pierwsza\n";
  }
#endif
